var contacts = angular.module("contacts", []);

contacts.controller("ListCtrl", ["$scope", "$filter", function($scope, $filter) {
	var orderBy = $filter('orderBy');

	$scope.contactList = [
		{"name": "Ilyene Dietz", "phone": "8998554", "provider": {"name": "Vivo", "code": 15}},
		{"name": "Felix Dietz", "phone": "33225566", "provider": {"name": "TIM", "code": 14}},
		{"name": "José Aristeu da Silva", "phone": "66552211", "provider": {"name": "Vivo", "code": 15}},
		{"name": "Suzane Dietz", "phone": "99558877", "provider": {"name": "Oi", "code": 20}},
		{"name": "Frederico Dietz", "phone": "33221166", "provider": {"name": "TIM", "code": 14}},
		{"name": "Carlos Henrique", "phone": "77884455", "provider": {"name": "Vivo", "code": 15}},
		{"name": "Pedro Mura", "phone": "33669988", "provider": {"name": "Oi", "code": 20}}
	];

	$scope.providers = [
		{"name": "Vivo", "code": 15},
		{"name": "TIM", "code": 14},
		{"name": "Oi", "code": 20}
	];

	var reverse = false;
	$scope.order = function(predicate) {
		$scope.contactList = orderBy($scope.contactList, predicate, reverse);
		reverse = !reverse;
	}

	$scope.order('name');

	$scope.addContact = function(contact) {
		$scope.contactList.push(angular.copy(contact));
		delete $scope.contact;
	}

	$scope.deleteContacts = function(contacts) {
		$scope.contactList = contacts.filter(function(contact) {
			return !contact.selected;
		});
	}

	$scope.hasSelectedContact = function(contacts) {
		return contacts.some(function(contact) {
			return contact.selected;
		});
	}
}]);